using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public bool canJump ;
    public Rigidbody2D rb;
    public float speed = 5f;
    public float direction = 0f;
    public float jumpForce = 5f;

    public LayerMask capa;
    void Start()
    {
       rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        Animations();
        #region Teclas Movimiento

        /*
        if (Input.GetKey("a"))
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-500 * Time.deltaTime, 0));
            if (canJump) 
            {
                gameObject.GetComponent<Animator>().SetBool("moving", true); 
            }
            else 
            {
                gameObject.GetComponent<Animator>().SetBool("moving", false);
                gameObject.GetComponent<Animator>().SetBool("jumping", true); 
            }
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }
        if (Input.GetKey("d"))
        {
            if (canJump)
            {
                gameObject.GetComponent<Animator>().SetBool("moving", true);
            }
            else
            {
                gameObject.GetComponent<Animator>().SetBool("moving", false);
                gameObject.GetComponent<Animator>().SetBool("jumping", true);
            }
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(500 * Time.deltaTime, 0));
            
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }
        if(!Input.GetKey("a") && !Input.GetKey("d"))
        {
            gameObject.GetComponent<Animator>().SetBool("moving", false);
        }

        */
        #endregion


        /*
        if (Input.GetKeyDown("space") && canJump)
        {
            canJump = false;
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, JumpForce * Time.deltaTime));
            gameObject.GetComponent<Animator>().SetBool("jumping", true);
        }

        if(Input.GetKey("a")  && Input.GetKeyDown("space"))
        {
            gameObject.GetComponent<Animator>().SetBool("jumping", true);
        }
        if (Input.GetKey("d") && Input.GetKeyDown("space"))
        {
            gameObject.GetComponent<Animator>().SetBool("jumping", true);
        }
        */

    }

    void FixedUpdate()
    {
        
        rb.velocity = new Vector2(direction * speed, rb.velocity.y);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "ground")
        {
            canJump = true;
            gameObject.GetComponent<Animator>().SetBool("jumping", false);
        }
    }
    private void Animations()
    {
        if (Input.GetKey("a"))
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }
        if (Input.GetKey("d"))
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }



        if (Input.GetKey("a")&& canJump)
        {
            gameObject.GetComponent<Animator>().SetBool("moving", true);
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }
        else 
        { 
            gameObject.GetComponent<Animator>().SetBool("moving", false); 
        }
        if (Input.GetKey("d") && canJump)
        {
            gameObject.GetComponent<Animator>().SetBool("moving", true);
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }
        else 
        { 
            gameObject.GetComponent<Animator>().SetBool("moving", false); 
        }

        if(Input.GetKey("a") || Input.GetKey("d"))
        {
            if (canJump)
            {
                if (Input.GetKey("a"))
                {
                    gameObject.GetComponent<Animator>().SetBool("moving", true);
                    gameObject.GetComponent<SpriteRenderer>().flipX = true;
                }
                if (Input.GetKey("d"))
                {
                    gameObject.GetComponent<Animator>().SetBool("moving", true);
                    gameObject.GetComponent<SpriteRenderer>().flipX = false;
                }

            }
            else
            {
                gameObject.GetComponent<Animator>().SetBool("moving", false);
            }
        }
        if(Input.GetKey("a") && Input.GetKey("d"))
        {
            gameObject.GetComponent<Animator>().SetBool("moving", false);
        }


        
    }
    private void Movement()
    {
        direction = Input.GetAxisRaw("Horizontal");
        Debug.DrawLine(transform.position, transform.position + Vector3.down * 17f, Color.green);

        if(Physics2D.Raycast(transform.position, Vector2.down, 17f, capa))
        {
            canJump = true;
            if (Input.GetButtonDown("Jump"))
            {
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                gameObject.GetComponent<Animator>().SetBool("jumping", true);

            }
        }
        else { canJump = false; }
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoyoteTime : MonoBehaviour
{
    [SerializeField] LayerMask floor;
    void Update()
    {
        RaycastHit2D raycastSuelo = Physics2D.Raycast(transform.position, Vector2.down, 0.25f, floor);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(raycastSuelo == true)
            {
                gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 50000 * Time.deltaTime));
                gameObject.GetComponent<Animator>().SetBool("jumping", true);
            }
            //else
            //{
            //    gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 50000 * Time.deltaTime)); ;
            //}
        }
    }
}
